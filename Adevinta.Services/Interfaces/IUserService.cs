﻿using Adevinta.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adevinta.Services.Interfaces
{
    public interface IUserService
    {
        IEnumerable<UserDTO> GetUsers();

        UserDTO GetUserByUsername(string username);

        bool IsValid(string username, string password, List<string> roleNames);

        bool IsValid(UserDTO user, List<string> roleNames);

        UserDTO LogIn(string username, string password);

        void Add(UserDTO user);

        void Update(UserDTO user);

        void Delete(string username);

    }
}
