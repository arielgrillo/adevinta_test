﻿using Adevinta.DataAccess.Database;
using Adevinta.DataAccess.Interfaces;
using Adevinta.Services.DTOs;
using Adevinta.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adevinta.Services
{
    public class UserService : ServiceBase, IUserService
    {
        private IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public void Add(UserDTO user)
        {
            var userDb = Mapper.Map<UserDB>(user);
            _userRepository.Add(userDb);
        }

        public void Delete(string username)
        {
            _userRepository.Delete(username);
        }

        public UserDTO GetUserByUsername(string username)
        {
            var user = _userRepository.GetByUsername(username);
            return Mapper.Map <UserDTO>(user);
        }

        public IEnumerable<UserDTO> GetUsers()
        {
            var users = _userRepository.GetAll();
            return Mapper.Map<List<UserDTO>>(users);
        }

        public bool IsValid(string username, string password, List<string> roleNames)
        {
            var user = _userRepository.Get(username, password);
            if(user != null && roleNames?.Count > 0)
            {
                return user.Roles.Any(ur => roleNames.Any(r => r == ur.Name));
            }
            else
            {
                //If there are no roles, only it's necessary review user credential.
                return user != null;
            }
        }

        public bool IsValid(UserDTO user, List<string> roleNames)
        {
            if (user == null || user.Roles == null || user.Roles.Count <= 0) return false;

            return IsValid(user.UserName, user.Password, roleNames);

        }

        public UserDTO LogIn(string username, string password)
        {
            var user = _userRepository.Get(username, password);

            if(user!= null)
            {
                return Mapper.Map<UserDTO>(user);
            }
            return null;
        }

        public void Update(UserDTO user)
        {
            var userDb = Mapper.Map<UserDB>(user);
            _userRepository.Update(userDb);
        }

    }
}
