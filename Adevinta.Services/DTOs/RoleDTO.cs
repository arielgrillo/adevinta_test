﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adevinta.Services.DTOs
{
    public class RoleDTO
    {
        public string Name { get; set; }

        public List<string> AllowedControllers { get; set; }
    }
}
