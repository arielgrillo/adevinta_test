﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adevinta.Services.DTOs
{
    public class UserDTO
    {
        public string UserName { get; set; }
    
        public string Password {get; set; }

        public List<RoleDTO> Roles { get; set; }

    }
}
