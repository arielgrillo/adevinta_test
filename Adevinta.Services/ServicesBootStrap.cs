﻿using Adevinta.DataAccess;
using Adevinta.Services.Interfaces;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adevinta.Services
{
    public class ServicesBootStrap
    {
        public static void RegisterServices(Container container)
        {
            container.Register<IUserService, UserService>(Lifestyle.Singleton);

            DataAccessBootStrap.RegisterRepositories(container);
        }
    }
}
