﻿using Adevinta.DataAccess.Database;
using Adevinta.Services.DTOs;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adevinta.Services
{
    public class ServiceBase
    {
        protected IMapper Mapper { get; private set; }

        public ServiceBase()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserDB, UserDTO>().ReverseMap();
                cfg.CreateMap<RoleDB, RoleDTO>().ReverseMap();
            });
            configuration.AssertConfigurationIsValid();
            Mapper = configuration.CreateMapper();
        }
    }
}
