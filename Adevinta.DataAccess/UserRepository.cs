﻿using Adevinta.DataAccess.Database;
using Adevinta.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adevinta.DataAccess
{
    public class UserRepository : IUserRepository
    {
        public void Add(UserDB user)
        {
            if (GetByUsername(user.UserName) != null)
            {
                throw new Exception("Username already in use.");
            }
            DatabaseStub.Users.Add(user);
        }

        public UserDB Get(string username, string password)
        {
            return DatabaseStub.Users.Where(u => u.UserName.Equals(username) && u.Password.Equals(password)).FirstOrDefault();
        }

        public IEnumerable<UserDB> GetAll()
        {
            return DatabaseStub.Users.ToList();
        }

        public UserDB GetByUsername(string username)
        {
            return DatabaseStub.Users.Where(u => u.UserName.Equals(username)).FirstOrDefault();
        }

        public void Update(UserDB user)
        {
            var userInDb = Get(user.UserName, user.Password);
            if (userInDb != null)
            {
                userInDb.UserName = user.UserName;
                userInDb.Password = user.Password;
                userInDb.Roles.Clear();
                userInDb.Roles.AddRange(user.Roles);
            }
            else
            {
                throw new Exception("User not found");
            }

        }

        public void Delete(string username)
        {
            var userInDb = GetByUsername(username);
            if (userInDb != null)
            {
                DatabaseStub.Users.Remove(userInDb);
            }
            else
            {
                throw new Exception("User not found");
            }

        }
    }
}
