﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adevinta.DataAccess.Database
{
    public class UserDB
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public List<RoleDB> Roles { get; set; }
    }
}
