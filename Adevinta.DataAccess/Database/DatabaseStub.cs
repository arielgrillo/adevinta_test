﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adevinta.DataAccess.Database
{
    public class DatabaseStub
    {
        private static List<UserDB> _users;
        public static List<UserDB> Users { get { return BuildUserStubs(); } }

        private static List<UserDB> BuildUserStubs()
        {
            if (_users == null)
            {
                _users = new List<UserDB>
                {
                    new UserDB { UserName = "admin", Password = "admin", Roles = CreateRolesAdmin() },
                    new UserDB { UserName = "user1", Password = "user1", Roles = CreateRolesUSer1() },
                    new UserDB { UserName = "user2", Password = "user2", Roles = CreateRolesUSer2() },
                    new UserDB { UserName = "user3", Password = "user3", Roles = CreateRolesUSer3() }
                };
            }
            return _users;

        }

        private static List<RoleDB> CreateRolesUSer1()
        {
            return new List<RoleDB>
            {
                new RoleDB
                {
                    Name = "PAGE_1",
                    AllowedControllers = new List<string> {"Page1"}
                }
            };
        }

        private static List<RoleDB> CreateRolesUSer2()
        {
            return new List<RoleDB>
            {
                new RoleDB
                {
                    Name = "PAGE_2",
                    AllowedControllers = new List<string> {"Page2"}
                }
            };
        }

        private static List<RoleDB> CreateRolesUSer3()
        {
            return new List<RoleDB>
            {
                new RoleDB
                {
                    Name = "PAGE_3",
                    AllowedControllers = new List<string> {"Page3" +
                    ""}
                }
            };
        }

        private static List<RoleDB> CreateRolesAdmin()
        {
            return new List<RoleDB>
            {
                new RoleDB
                {
                    Name = "admin",
                    AllowedControllers = new List<string> {"Home","Page1","Page2","Page3"}
                }
            };
        }
    }
}
