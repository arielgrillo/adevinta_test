﻿using Adevinta.DataAccess.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adevinta.DataAccess.Interfaces
{
    public interface IUserRepository
    {
        IEnumerable<UserDB> GetAll();

        UserDB Get(string username, string password);

        UserDB GetByUsername(string username);

        void Add(UserDB user);

        void Update(UserDB user);

        void Delete(string username);

    }
}
