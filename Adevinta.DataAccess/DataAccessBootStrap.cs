﻿using Adevinta.DataAccess.Interfaces;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adevinta.DataAccess
{
    public class DataAccessBootStrap
    {
        public static void RegisterRepositories(Container container)
        {
            container.Register<IUserRepository, UserRepository>(Lifestyle.Singleton);

        }

    }
}
