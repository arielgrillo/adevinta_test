Adevinta - Technical Test

1 - In order to start playing with the Applciation, you have to open it with Visual Studio and press F5 in order to start it.

2 - The login is the first page that you'll see, so you can enter the corresponding credentials and you'll be redirect to the corresponding page.
	A - Admin: user: "admin" and password: "admin", landing page "Home"
	B - User1: user: "user1" and password: "user1", landing page "Page1"
	C - User1: user: "user2" and password: "user2", landing page "Page2"
	D - User1: user: "user3" and password: "user3", landing page "Page3"

3 - Once the server starts there are exposed the following end points. 
Admin user has read/write permissions in all of them and the rest of users only can read/access to GetUser and GetAll endpoints:
	A - GET: http://[server]:[port]/api/User
	B - GET: http://[server]:[port]/api/User/[username]
	C - POST: http://[server]:[port]/api/User
	D - PUT: http://[server]:[port]/api/User	
	E - DELETE: http://localhost:52700/api/User/User4

4 - Also in the repository you'll find a Postman collection file in order to have the mentioned endpoints for test.

5 - In order to run tests you can go to Tets => Run => All tests in visual studio. The test project is under "Test" solution folder