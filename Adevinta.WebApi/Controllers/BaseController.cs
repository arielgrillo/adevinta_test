﻿using Adevinta.Services.DTOs;
using Adevinta.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Adevinta.WebApi.Controllers
{
    public class BaseController : Controller
    {
        protected List<string> ValidRoles { get; set; }

        IUserService _userService;
        public BaseController(IUserService userService)
        {
            _userService = userService;
        }

        public ActionResult Index()
        {
            var user = Session["User"] as UserDTO;
            var isValid = _userService.IsValid(user, ValidRoles);
            if (isValid)
            {
                return View();
            }
            else
            {
                return RedirectToAction("AccesDenied", "Login");
            }

        }
    }
}