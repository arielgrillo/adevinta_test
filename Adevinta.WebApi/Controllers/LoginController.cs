﻿using Adevinta.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace Adevinta.WebApi.Controllers
{
    public class LoginController : Controller
    {
        private IUserService _userService;

        public LoginController(IUserService userService)
        {
            _userService = userService;
        }
        public ActionResult Index()
        {
            ViewBag.Title = "Login";

            return View();
        }

        [HttpPost]
        public JsonResult LogIn(string username, string password)
        {
            var user = _userService.LogIn(username, password);
            if(user != null)
            {
                FormsAuthentication.SetAuthCookie(user.UserName, false);
                Session["User"] = user;

                return Json(new { state = 1, returnUrl = "/" + user.Roles[0].AllowedControllers[0] + "/Index" });
            }
            return Json(new { state = -1});
            
        }
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            return RedirectToAction("Index", "Login");
        }

        public ActionResult AccesDenied()
        {
            return View();
        }
    }
}