﻿using Adevinta.Services.DTOs;
using Adevinta.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Adevinta.WebApi.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public HomeController(IUserService userService) : base(userService)
        {
            ValidRoles = new List<string> { "admin" };
        }


    }
}
