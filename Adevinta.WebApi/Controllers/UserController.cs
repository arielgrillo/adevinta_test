﻿using Adevinta.Services.DTOs;
using Adevinta.Services.Interfaces;
using Adevinta.WebApi.Filters;
using Adevinta.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Adevinta.WebApi.Controllers
{
    public class UserController : ApiController
    {
        private IUserService _userService; 

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        public IHttpActionResult Get()
        {
            var usersModel = new List<UserModel>();
            var users = _userService.GetUsers().ToList();
            usersModel.AddRange(users.Select(u => 
                new UserModel {
                    Roles = u.Roles.Select(r => new RoleModel { Name = r.Name, AllowedControllers=r.AllowedControllers }).ToList(),
                    Username = u.UserName }).ToList());

            return Ok(usersModel);
        }

        [Route("api/User/{Username}")]
        public IHttpActionResult GetUser(string username)
        {
            var user = _userService.GetUserByUsername(username);
            if(user != null)
            {
                var userModel = new UserModel {
                    Username = user.UserName,
                    Roles = user.Roles.Select(r => new RoleModel { Name = r.Name, AllowedControllers = r.AllowedControllers }).ToList(),
                };
                return Ok(userModel);
            }
            else
            {
                return BadRequest("User not found");
            }
        }


        [BasicAuthentication(new string[] { "admin" })]
        public IHttpActionResult Post([FromBody] UserDTO user)
        {
            try
            {
                _userService.Add(user);
                return Ok(user);
            }
            catch(Exception ex)
            {
                return BadRequest("Errors to update user." + ex.Message);
            }
        }

        [BasicAuthentication(new string[] { "admin" })]
        public IHttpActionResult Put([FromBody] UserDTO user)
        {
            try
            {
                _userService.Update(user);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return BadRequest("Errors to update user." + ex.Message);
            }
        }

        [BasicAuthentication(new string[] { "admin" })]
        [Route("api/User/{Username}")]
        public IHttpActionResult Delete(string username)
        {
            try
            {
                _userService.Delete(username);
                return Ok(username);
            }
            catch (Exception ex)
            {
                return BadRequest("Errors to update user." + ex.Message);
            }
        }


    }
}
