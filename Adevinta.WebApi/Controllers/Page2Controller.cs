﻿using Adevinta.Services.DTOs;
using Adevinta.Services.Interfaces;
using Adevinta.WebApi.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Security;

namespace Adevinta.WebApi.Controllers
{
    [System.Web.Http.Authorize]
    public class Page2Controller : BaseController
    {
        public Page2Controller(IUserService userService) : base(userService)
        {
            ValidRoles = new List<string> { "PAGE_2", "admin" };
        }


    }
}