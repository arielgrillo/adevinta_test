﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using SimpleInjector;
using SimpleInjector.Lifestyles;
using SimpleInjector.Integration.WebApi;
using Adevinta.Services.Interfaces;
using Adevinta.Services;
using Adevinta.WebApi.Filters;
using SimpleInjector.Integration.Web.Mvc;
using System.Web.Mvc;

namespace Adevinta.WebApi
{
    public class DependencyInjectionConfig
    {
        private static Container _container;

        public static void Register()
        {
            _container = new Container();
            _container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            ServicesBootStrap.RegisterServices(_container);

            _container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
            _container.RegisterMvcControllers(System.Reflection.Assembly.GetExecutingAssembly());

            _container.Verify();

            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(_container);
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(_container));
        }

        public static T GetInstance<T>()
        {
            return (T)_container.GetInstance(typeof(T));
        }
    }
}