﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adevinta.WebApi.Models
{
    public class UserModel
    {
        public string Username { get; set; }

        public List<RoleModel> Roles { get; set; }
    }
}