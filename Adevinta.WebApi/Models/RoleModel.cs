﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adevinta.WebApi.Models
{
    public class RoleModel
    {
        public string Name { get; set; }

        public List<string> AllowedControllers { get; set; }
    }
}