﻿using System;
using System.Collections.Generic;
using System.Linq;
using Adevinta.DataAccess.Database;
using Adevinta.DataAccess.Interfaces;
using Adevinta.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Adevinta.Services.Tests
{
    [TestClass]
    public class UserServiceTests:BaseTest
    {
        [TestMethod]
        public void GetUsers()
        {
            //Arrange
            var userRepo = Substitute.For<IUserRepository>();
            userRepo.GetAll().Returns(BuildUsers());

            //Act
            var userService = new UserService(userRepo);
            var users = userService.GetUsers().ToList();

            //Assert
            userRepo.Received().GetAll();
            Assert.AreEqual(2, users.Count);
        }

        [TestMethod]
        public void GetUserLogin()
        {
            //Arrange
            var userRepo = Substitute.For<IUserRepository>();
            userRepo.Get("u2", "u2").Returns(BuildUser());

            //Act
            var userService = new UserService(userRepo);
            var user = userService.LogIn("u2", "u2");

            //Assert
            userRepo.Received().Get("u2", "u2");
            Assert.IsNotNull(user);
            Assert.AreEqual("u2", user.UserName);
            Assert.AreEqual("u2", user.Password);
            Assert.AreEqual(1, user.Roles.Count);
            Assert.AreEqual("r2", user.Roles[0].Name);
            Assert.AreEqual(1, user.Roles[0].AllowedControllers.Count);
            Assert.AreEqual("r2Controller", user.Roles[0].AllowedControllers[0]);
        }

        [TestMethod]
        public void GetUserByUsername()
        {
            //Arrange
            var userRepo = Substitute.For<IUserRepository>();
            userRepo.GetByUsername("u2").Returns(BuildUser());

            //Act
            var userService = new UserService(userRepo);
            var user = userService.GetUserByUsername("u2");

            //Assert
            userRepo.Received().GetByUsername("u2");
            Assert.IsNotNull(user);
            Assert.AreEqual("u2", user.UserName);
            Assert.AreEqual("u2", user.Password);
            Assert.AreEqual(1, user.Roles.Count);
            Assert.AreEqual("r2", user.Roles[0].Name);
            Assert.AreEqual(1, user.Roles[0].AllowedControllers.Count);
            Assert.AreEqual("r2Controller", user.Roles[0].AllowedControllers[0]);
        }

        [TestMethod]
        public void IsValid()
        {
            //Arrange
            var userRepo = Substitute.For<IUserRepository>();
            userRepo.Get(Arg.Any<string>(), Arg.Any<string>()).Returns(BuildUser());

            //Act
            var userService = new UserService(userRepo);
            var isValid = userService.IsValid("u2", "u2", new List<string> { "r2" });

            //Assert
            userRepo.Received().Get(Arg.Any<string>(), Arg.Any<string>());
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void IsValidDTO()
        {
            //Arrange
            var userRepo = Substitute.For<IUserRepository>();
            userRepo.Get(Arg.Any<string>(), Arg.Any<string>()).Returns(BuildUser());
            var user = new UserDTO() { UserName = "u2", Password = "u2", Roles = new List<RoleDTO> { new RoleDTO { Name = "r2"} } };

            //Act
            var userService = new UserService(userRepo);
            var isValid = userService.IsValid(user, new List<string> { "r2" });

            //Assert
            userRepo.Received().Get(Arg.Any<string>(), Arg.Any<string>());
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void IsNotValidDTO()
        {
            //Arrange
            var userRepo = Substitute.For<IUserRepository>();
            userRepo.Get(Arg.Any<string>(), Arg.Any<string>()).Returns(BuildUser());
            var user = new UserDTO() { UserName = "u2", Password = "u2", Roles = new List<RoleDTO> { new RoleDTO { Name = "r2" } } };

            //Act
            var userService = new UserService(userRepo);
            var isValid = userService.IsValid(user, new List<string> { "rnopass" });

            //Assert
            userRepo.Received().Get(Arg.Any<string>(), Arg.Any<string>());
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void IsNotValid()
        {
            //Arrange
            var userRepo = Substitute.For<IUserRepository>();
            userRepo.Get("u2", "u2").Returns(BuildUser());

            //Act
            var userService = new UserService(userRepo);
            var isValid = userService.IsValid("u2", "nopass", new List<string> { "r2" });

            //Assert
            userRepo.Received().Get(Arg.Any<string>(), Arg.Any<string>());
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void IsValid_NoRole()
        {
            //Arrange
            var userRepo = Substitute.For<IUserRepository>();
            userRepo.Get(Arg.Any<string>(), Arg.Any<string>()).Returns(BuildUser());

            //Act
            var userService = new UserService(userRepo);
            var isValid = userService.IsValid("u2", "u2", new List<string> { });

            //Assert
            userRepo.Received().Get(Arg.Any<string>(), Arg.Any<string>());
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void IsNotValid_Role()
        {
            //Arrange
            var userRepo = Substitute.For<IUserRepository>();
            userRepo.Get(Arg.Any<string>(), Arg.Any<string>()).Returns(BuildUser());

            //Act
            var userService = new UserService(userRepo);
            var isValid = userService.IsValid("u2", "u2", new List<string> { "r3" });

            //Assert
            userRepo.Received().Get(Arg.Any<string>(), Arg.Any<string>());
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void Update()
        {
            //Arrange
            var user = new UserDTO { UserName = "x", Password = "x", Roles = new List<RoleDTO> { new RoleDTO { Name = "rx" } } };
            var userRepo = Substitute.For<IUserRepository>();
            userRepo.Update(Arg.Any<UserDB>());

            //Act
            var userService = new UserService(userRepo);
            userService.Update(user);

            //Assert
            userRepo.Received().Update(Arg.Any<UserDB>());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void UpdateNotOk()
        {
            //Arrange
            var user = new UserDTO { UserName = "x", Password = "x", Roles = new List<RoleDTO> { new RoleDTO { Name = "rx" } } };
            var userRepo = Substitute.For<IUserRepository>();
            userRepo
                .When(x => x.Update(Arg.Any<UserDB>()))
                .Do(x => { throw new Exception(); });

            //Act
            var userService = new UserService(userRepo);
            userService.Update(user);

            //Assert
            userRepo.Received().Update(Arg.Any<UserDB>());
        }

        [TestMethod]
        public void Add()
        {
            //Arrange
            var user = new UserDTO { UserName = "x", Password = "x", Roles = new List<RoleDTO> { new RoleDTO { Name = "rx" } } };
            var userRepo = Substitute.For<IUserRepository>();
            userRepo.Add(Arg.Any<UserDB>());

            //Act
            var userService = new UserService(userRepo);
            userService.Add(user);

            //Assert
            userRepo.Received().Add(Arg.Any<UserDB>());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void AddNotOk()
        {
            //Arrange
            var user = new UserDTO { UserName = "x", Password = "x", Roles = new List<RoleDTO> { new RoleDTO { Name = "rx" } } };
            var userRepo = Substitute.For<IUserRepository>();
            userRepo
                .When(x => x.Add(Arg.Any<UserDB>()))
                .Do(x => { throw new Exception(); });

            //Act
            var userService = new UserService(userRepo);
            userService.Add(user);

            //Assert
            userRepo.Received().Add(Arg.Any<UserDB>());
        }

        [TestMethod]
        public void Delete()
        {
            //Arrange
            var user = new UserDTO { UserName = "x", Password = "x", Roles = new List<RoleDTO> { new RoleDTO { Name = "rx" } } };
            var userRepo = Substitute.For<IUserRepository>();
            userRepo.Delete(Arg.Any<string>());

            //Act
            var userService = new UserService(userRepo);
            userService.Delete("x");

            //Assert
            userRepo.Received().Delete("x");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void DeleteNotOk()
        {
            //Arrange
            var user = new UserDTO { UserName = "x", Password = "x", Roles = new List<RoleDTO> { new RoleDTO { Name = "rx" } } };
            var userRepo = Substitute.For<IUserRepository>();
            userRepo
                .When(x => x.Delete(Arg.Any<string>()))
                .Do(x => { throw new Exception(); });

            //Act
            var userService = new UserService(userRepo);
            userService.Delete("x");

            //Assert
            userRepo.Received().Delete("x");
        }

        private IEnumerable<UserDB> BuildUsers()
        {
            var users = new List<UserDB>
            {
                new UserDB{UserName="a1", Password="a1",Roles=new List<RoleDB>{new RoleDB{Name = "admin",AllowedControllers= new List<string> {"home" } }} },
                new UserDB{UserName="u1", Password="u1",Roles=new List<RoleDB>{new RoleDB{Name = "r1",AllowedControllers= new List<string> {"r1Controller" } }} }
            };

            return users;
        }

        private UserDB BuildUser()
        {
            return new UserDB { UserName = "u2", Password = "u2", Roles = new List<RoleDB> { new RoleDB { Name = "r2", AllowedControllers = new List<string> { "r2Controller" } } } };
        }

    }
}
