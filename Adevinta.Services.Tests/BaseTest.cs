﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleInjector;
using SimpleInjector.Lifestyles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adevinta.Services.Tests
{

    public class BaseTest
    {
        protected static Container _container;

        [TestInitialize]
        public void Init()
        {
            _container = new Container();
            _container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            ServicesBootStrap.RegisterServices(_container);
            _container.Verify();

        }
    }
}
